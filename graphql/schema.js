export const typeDefs = `
  type Beer {
    id:           Int
    name:         String
    brewery:      String
    menuOverride: String
    type:         String
    price:        Float
    volume:       Float
    category:     String
    isAvailable:  Boolean
    isArchived:   Boolean
    alcohol:      Float
    onTap:        Boolean
    comments:     [Comment]
    ratings:      [Rating]
    flavours:     [Flavour]
  }

  type Cocktail {
    id:              Int
    name:            String
    ingredients:     [CocktailsOnIngredients]
    tags:            [CocktailsOnTags]
    price:           Float
    containsAlcohol: Boolean
    isArchived:      Boolean
    imagePortrait:   String
    imageLandscape:  String
  }

  type Spirit {
    id:          Int
    name:        String
    type:        String
    price:       Float
    alcohol:     Float
    tags:        [SpiritsOnTags]
    isAvailable: Boolean
    isArchived:  Boolean
  }

  type Ingredient {
    name:        String
    isAvailable: Boolean
    cocktail:    [CocktailsOnIngredients]
  }

  type CocktailsOnIngredients {
    cocktail:   Cocktail
    ingredient: Ingredient
  }

  type Tag {
    name: String
  }

  type CocktailsOnTags {
    cocktail: Cocktail
    tag:      Tag
  }

  type SpiritsOnTags {
    spirit: Spirit
    tag:    Tag
  }

  type Post {
    id:        Int
    date:      String
    submitter: String
    title:     String
    content:   String
  }

  type Comment {
    id: Int
    date: String
    submitter: User
    comment: String
    beer: Beer
  }

  type User {
    sub: String
    name: String
  }

  type Rating {
    id: Int
    user: User
    rating: Float
    beer: Beer
  }

  type Flavour {
    id: Int
    user: User
    beer: Beer
    hoppy: Float
    malty: Float
    fruity: Float
    bitter: Float
    sour: Float
    sweet: Float
  }

  type Pub {
    id: Int
    host: String
    date: String
    updatedAt: String
  }

  type Query {
    getBeers: [Beer]
    getAvailableBeers: [Beer]
    getBeerById(id: Int): Beer
    getCocktails: [Cocktail]
    getAvailableCocktails: [Cocktail]
    getSpirits: [Spirit]
    getAvailableSpirits: [Spirit]
    getIngredients: [Ingredient]
    getTags: [Tag]
    getArchivedBeers: [Beer]
    getArchivedCocktails: [Cocktail]
    getArchivedSpirits: [Spirit]
    getPosts(skip: Int, take: Int): [Post]
    getNewestPost: Post
    getPostCount: Int
    export(name: String, brewery: String): String
    getComments(beerId: Int): [Comment]
    getPub: Pub
  }

  type Mutation {
    addBeer(name: String, brewery: String, menuOverride:String, type: String, price: Float, volume: Float, category: String, isAvailable: Boolean, alcohol: Float, onTap: Boolean): Boolean
    deleteBeer(id: Int): Boolean
    editBeer(id: Int, name: String, brewery: String, menuOverride:String, type: String, price: Float, volume: Float, category: String, isAvailable: Boolean, alcohol: Float, onTap: Boolean): Boolean
    updateAvailability(id: Int, status: Boolean): Boolean
    addCocktail(name: String, ingredients: [String], tags: [String], price: Float, containsAlcohol: Boolean, imagePortrait: String, imageLandscape: String): Boolean
    editCocktail(id: Int, name: String, ingredients: [String], tags: [String], price: Float, containsAlcohol: Boolean, imagePortrait: String, imageLandscape: String): Boolean
    deleteCocktail(id: Int): Boolean
    updateIngredientAvailability(name: String, status: Boolean): Boolean
    addSpirit(name: String, type: String, price: Float, alcohol: Float, tags: [String], isAvailable: Boolean, isArchived: Boolean): Boolean
    editSpirit(id: Int, name: String, type: String, price: Float, alcohol: Float, tags: [String], isAvailable: Boolean, isArchived: Boolean): Boolean
    deleteSpirit(id: Int): Boolean
    updateSpiritAvailability(id: Int, status: Boolean): Boolean
    updateBeerArchive(id: Int, status: Boolean): Boolean
    updateCocktailArchive(id: Int, status: Boolean): Boolean
    updateSpiritArchive(id: Int, status: Boolean): Boolean
    addPost(title: String, content: String): Boolean
    sendPost(title: String, content: String, receivers: [String]): Boolean
    deletePost(id: Int): Boolean
    import(excelLine: String): Boolean
    addComment(comment: String, beerId: Int): Boolean
    deleteComment(id: Int): Boolean
    addOrUpdateRating(beerId: Int, rating: Float): Boolean
    addOrUpdateFlavour(beerId: Int, sweet: Float, sour: Float, bitter: Float, malty: Float, hoppy: Float, fruity: Float): Boolean
    updatePub(host: String, date: String): Boolean
  }
`;
