#!/bin/bash

echo "DATABASE_URL=mysql://$SIP_MYSQL_PPLW_USER:$SIP_MYSQL_PPLW_PW@$SIP_MYSQL_PPLW_SERVER:$SIP_MYSQL_PPLW_PORT/$SIP_MYSQL_PPLW_NAME?schema=public" > .env

npx prisma generate
npx prisma migrate deploy

echo "Updating S3 Bucket Policy"
curl -s -X POST $NEXTAUTH_URL/api/image/updateBucketPolicy
