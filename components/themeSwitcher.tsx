import { Switch, useMantineTheme, useMantineColorScheme } from "@mantine/core";

import { Icon, ICONS } from "vseth-canine-ui";

export default function ThemeSwitcher() {
  const theme = useMantineTheme();
  const { colorScheme, toggleColorScheme } = useMantineColorScheme();
  const dark = colorScheme === "dark";

  return (
    <Switch
      size="md"
      color="vsethMain"
      offLabel={
        <Icon
          icon={ICONS.SUN}
          size={16}
          color={theme.colors[theme.primaryColor][theme.primaryShade]}
        />
      }
      onLabel={<Icon icon={ICONS.MOON} size={16} color="#ffffff" />}
      checked={dark}
      onChange={() => {
        console.log(colorScheme);
        toggleColorScheme();
      }}
    />
  );
}
