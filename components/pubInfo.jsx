import { useRouter } from "next/router";

import { useState, useEffect } from "react";

import { Alert, Button, Modal, Text, TextInput } from "@mantine/core";

import { IconBeer, IconEdit } from "@tabler/icons-react";

import { useTranslation, useLocale } from "next-i18next";

import { useSession } from "next-auth/react";

import hasAccess from "../utilities/hasAccess";
import { formatDate } from "../utilities/dates";

import { gql, useQuery, useMutation } from "@apollo/client";

const getPubQuery = gql`
  query getPub {
    getPub {
      host
      date
      updatedAt
    }
  }
`;

const updatePubMutation = gql`
  mutation updatePub($host: String, $date: String) {
    updatePub(host: $host, date: $date)
  }
`;

export default function PubInfo() {
  const { data, refetch } = useQuery(getPubQuery);
  const [updatePub] = useMutation(updatePubMutation);

  const { t } = useTranslation("common");
  const { locale } = useRouter();
  const { data: session } = useSession();

  const [open, setOpen] = useState(false);
  const [host, setHost] = useState("");
  const [date, setDate] = useState("");

  useEffect(() => {
    if (!data?.getPub) return;
    setHost(data.getPub.host);
    setDate(data.getPub.date);
  }, [data]);

  if (!data?.getPub) return <></>;
  if (!data.getPub.date && !hasAccess(session, true)) return <></>;

  const handleSubmit = async (e) => {
    e.preventDefault();

    await updatePub({
      variables: {
        host,
        date,
      },
    });

    refetch();
    setOpen(false);
  };

  return (
    <>
      <Alert
        title={`${t("bannerPub")} ${data.getPub.date}`}
        icon={<IconBeer />}
        style={{ marginTop: "1rem" }}
      >
        {data.getPub.host && (
          <Text>
            {t("bannerHost")}{" "}
            <Text component="span" fw={700}>
              {data.getPub.host}
            </Text>
          </Text>
        )}
        <Text>{t("bannerCU")}</Text>
        <Text c="dimmed">
          {t("lastUpdate")} {formatDate(data.getPub.updatedAt, locale)}
        </Text>
        {hasAccess(session, true) && (
          <Button
            leftIcon={<IconEdit />}
            style={{ marginTop: "0.5rem" }}
            onClick={() => setOpen(!open)}
          >
            {t("edit")}
          </Button>
        )}
      </Alert>
      <Modal opened={open} onClose={() => setOpen(false)}>
        <form onSubmit={(e) => handleSubmit(e)}>
          <div style={{ textAlign: "center" }}>
            <h2>{t("editPub")}</h2>
          </div>

          <Alert title={t("note")}>{t("editPubNote")}</Alert>

          <TextInput
            label={t("date")}
            value={date}
            onChange={(e) => setDate(e.target.value)}
            style={{ marginTop: "0.5rem" }}
            withAsterisk
          />
          <TextInput
            label={t("host")}
            value={host}
            onChange={(e) => setHost(e.target.value)}
            style={{ marginTop: "0.5rem" }}
          />

          <Button type="submit" style={{ marginTop: "0.5rem" }}>
            {t("updatePub")}
          </Button>
        </form>
      </Modal>
    </>
  );
}
