import { useState, useEffect } from "react";

import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";

import { useSession, signIn } from "next-auth/react";

import {
  Accordion,
  Button,
  Container,
  Grid,
  Slider,
  Space,
} from "@mantine/core";

import PleaseLogIn from "../components/pleaseLogIn";

import { gql, useMutation } from "@apollo/client";

const addOrUpdateFlavourMutation = gql`
  mutation addOrUpdateFlavour(
    $beerId: Int!
    $hoppy: Float!
    $malty: Float!
    $fruity: Float!
    $bitter: Float!
    $sweet: Float!
    $sour: Float!
  ) {
    addOrUpdateFlavour(
      beerId: $beerId
      hoppy: $hoppy
      malty: $malty
      fruity: $fruity
      bitter: $bitter
      sweet: $sweet
      sour: $sour
    )
  }
`;

export default function FlavourForm({
  beerId,
  refetch,
  flavours,
  notify,
  update,
}) {
  const { t } = useTranslation("common");
  const { data: session } = useSession();

  let initHoppy = 3;
  let initMalty = 3;
  let initFruity = 3;
  let initBitter = 3;
  let initSweet = 3;
  let initSour = 3;

  const [hoppy, setHoppy] = useState(initHoppy);
  const [malty, setMalty] = useState(initMalty);
  const [fruity, setFruity] = useState(initFruity);
  const [bitter, setBitter] = useState(initBitter);
  const [sweet, setSweet] = useState(initSweet);
  const [sour, setSour] = useState(initSour);

  const [found, setFound] = useState([]);
  const [foundOne, setFoundOne] = useState(false);

  const reloadFlavours = () => {
    if (session && flavours) {
      const foundtmp = flavours.filter(
        (flavour) => flavour.user.sub == session.info.payload.sub
      );
      setFound(foundtmp);
      if (foundtmp.length > 0) {
        setFoundOne(true);
        setHoppy(foundtmp[0].hoppy);
        setMalty(foundtmp[0].malty);
        setFruity(foundtmp[0].fruity);
        setBitter(foundtmp[0].bitter);
        setSweet(foundtmp[0].sweet);
        setSour(foundtmp[0].sour);
      }
    }
  };

  useEffect(() => {
    reloadFlavours();
  }, []);

  const [addOrUpdateFlavour, { loading, error }] = useMutation(
    addOrUpdateFlavourMutation
  );

  const marks = [
    { value: 1, label: "1" },
    { value: 2, label: "2" },
    { value: 3, label: "3" },
    { value: 4, label: "4" },
    { value: 5, label: "5" },
  ];

  const submit = async () => {
    await addOrUpdateFlavour({
      variables: {
        beerId: beerId,
        hoppy,
        malty,
        fruity,
        bitter,
        sweet,
        sour,
      },
    });
    await refetch();
    notify(t("flavourAdded"), "", "green");
  };

  return (
    <Accordion.Item value="flavour">
      <Accordion.Control>
        <h2 style={{ margin: 0 }}>{t("flavourProfile")}</h2>
      </Accordion.Control>
      <Accordion.Panel>
        <Space h="md" />
        {session ? (
          <Grid>
            <Grid.Col md={6} sm={6} xs={12}>
              <Slider
                value={hoppy}
                onChange={setHoppy}
                label={t("hoppy")}
                labelAlwaysOn
                step={0.5}
                min={1}
                max={5}
                marks={marks}
                sx={{ marginTop: "40px" }}
              />
            </Grid.Col>
            <Grid.Col md={6} sm={6} xs={12}>
              <Slider
                value={malty}
                onChange={setMalty}
                label={t("malty")}
                labelAlwaysOn
                step={0.5}
                min={1}
                max={5}
                marks={marks}
                sx={{ marginTop: "40px" }}
              />
            </Grid.Col>
            <Grid.Col md={6} sm={6} xs={12}>
              <Slider
                value={fruity}
                onChange={setFruity}
                label={t("fruity")}
                labelAlwaysOn
                step={0.5}
                min={1}
                max={5}
                marks={marks}
                sx={{ marginTop: "40px" }}
              />
            </Grid.Col>
            <Grid.Col md={6} sm={6} xs={12}>
              <Slider
                value={bitter}
                onChange={setBitter}
                label={t("bitter")}
                labelAlwaysOn
                step={0.5}
                min={1}
                max={5}
                marks={marks}
                sx={{ marginTop: "40px" }}
              />
            </Grid.Col>
            <Grid.Col md={6} sm={6} xs={12}>
              <Slider
                value={sweet}
                onChange={setSweet}
                label={t("sweet")}
                labelAlwaysOn
                step={0.5}
                min={1}
                max={5}
                marks={marks}
                sx={{ marginTop: "40px" }}
              />
            </Grid.Col>
            <Grid.Col md={6} sm={6} xs={12}>
              <Slider
                value={sour}
                onChange={setSour}
                label={t("sour")}
                labelAlwaysOn
                step={0.5}
                min={1}
                max={5}
                marks={marks}
                sx={{ marginTop: "40px" }}
              />
            </Grid.Col>
            <Grid.Col md={6} sm={6} xs={12}>
              <Button onClick={submit} mt="md">
                {foundOne ? t("update") : t("submit")}
              </Button>
            </Grid.Col>
          </Grid>
        ) : (
          <>
            <PleaseLogIn />
          </>
        )}
        <Space h="md" />
      </Accordion.Panel>
    </Accordion.Item>
  );
}
