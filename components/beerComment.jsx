import React from "react";

import { useSession } from "next-auth/react";

import { useTranslation } from "next-i18next";

import { Accordion, Button, Container, Space, TextInput } from "@mantine/core";

import { useInputState } from "@mantine/hooks";

import PleaseLogIn from "../components/pleaseLogIn";

import { gql, useMutation } from "@apollo/client";

const addCommentMutation = gql`
  mutation addComment($comment: String!, $beerId: Int!) {
    addComment(comment: $comment, beerId: $beerId)
  }
`;

export default function BeerComment({ beerId, refetch, notify }) {
  const { data: session } = useSession();
  const [insertComment] = useMutation(addCommentMutation);

  const { t } = useTranslation("common");

  const [comment, setComment] = useInputState("");

  const submit = async (e) => {
    e.preventDefault();
    if (!session) return;
    if (comment == "") return;
    await insertComment({
      variables: {
        comment: comment,
        beerId: beerId,
      },
    });
    await refetch();
    setComment("");
    notify(t("commentAdded"), "", "green");
  };

  return (
    <Accordion.Item value="comment">
      <Accordion.Control>
        <h2 style={{ margin: 0 }}>{t("comment")}</h2>
      </Accordion.Control>
      <Accordion.Panel>
        <Space h="md" />
        {session ? (
          <form>
            <TextInput
              value={comment}
              onChange={setComment}
              placeholder={t("placeholder")}
            />
            <Button
              type="submit"
              style={{ marginTop: "10px" }}
              onClick={submit}
            >
              {t("submit")}
            </Button>
          </form>
        ) : (
          <>
            <PleaseLogIn />
          </>
        )}
        <Space h="md" />
      </Accordion.Panel>
    </Accordion.Item>
  );
}
