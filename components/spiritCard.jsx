import { ReactNode } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import { useTranslation } from "next-i18next";
import hasAccess from "../utilities/hasAccess";
import { useSession } from "next-auth/react";
import { useInterval } from "@mantine/hooks";
import {
  ActionIcon,
  Alert,
  Badge,
  Blockquote,
  Button,
  Card,
  createStyles,
  Group,
  List,
  Progress,
  Rating,
  Skeleton,
  Space,
  Switch,
  Text,
  TextInput,
  Tooltip,
  useMantineTheme,
} from "@mantine/core";

import {
  getTextColor,
  getAccentColor,
  getDisabledColor,
  getIconColor,
} from "../utilities/colors";

import { Icon, ICONS } from "vseth-canine-ui";
import { IconTrash } from "@tabler/icons-react";

import { gql, useMutation } from "@apollo/client";

const updateArchiveMutation = gql`
  mutation updateSpiritArchive($id: Int, $status: Boolean) {
    updateSpiritArchive(id: $id, status: $status)
  }
`;

const deleteSpiritMutation = gql`
  mutation deleteSpirit($id: Int) {
    deleteBeer(id: $id)
  }
`;

export default function SpiritCard({
  spirit,
  deleteSpirit,
  editSpirit,
  updateAvailability,
  refetch,
  isArchive,
}) {
  const [updateSpiritArchive] = useMutation(updateArchiveMutation);

  const { data: session } = useSession();
  const { t } = useTranslation("common");

  const theme = useMantineTheme();

  const toggleArchive = async () => {
    const res = await updateSpiritArchive({
      variables: {
        id: spirit.id,
        status: !spirit.isArchived,
      },
    });
    if (res) refetch();
  };

  const textColor = spirit.isAvailable
    ? getTextColor(theme)
    : getDisabledColor();
  const accentColor = spirit.isAvailable
    ? getAccentColor(theme)
    : getDisabledColor();

  const unsetSpirit = async () => {
    const res = await removeSpirit({
      variables: {
        id: beer.id,
      },
    });
    if (res) refetch();
  };

  return (
    <Card
      shadow="md"
      radius="md"
      style={{
        display: "flex",
        justifyContent: "space-between",
        flexDirection: "column",
        width: "100%",
        color: textColor,
      }}
      withBorder
    >
      <div>
        <h2 style={{ margin: 0 }}>{spirit.name}</h2>
        <h3>{spirit.type}</h3>
        <List spacing="xs" size="md" center style={{ color: textColor }}>
          <Group spacing={7} mt={7}>
            {spirit.tags.map((tag, i) => (
              <Badge key={i}>{tag.tag.name}</Badge>
            ))}
          </Group>
          <Space h="xl" />
          <List.Item
            icon={<Icon icon={ICONS.MONEY} color={accentColor} size={22} />}
          >
            <p>{spirit.price} CHF</p>
          </List.Item>
          <List.Item
            icon={<Icon icon={ICONS.SETTINGS} color={accentColor} size={22} />}
          >
            <p>{spirit.alcohol} %</p>
          </List.Item>
        </List>
      </div>
      <div>
        {hasAccess(session, true) && (
          <Group style={{ verticalAlign: "middle", marginTop: "20px" }}>
            {!isArchive && (
              <Button
                leftIcon={
                  <Icon icon={ICONS.EDIT} color={getIconColor(theme)} />
                }
                variant="light"
                onClick={() => editSpirit(spirit)}
              >
                {t("editSpirit")}
              </Button>
            )}
            {!isArchive && (
              <Switch
                checked={spirit.isAvailable}
                onChange={(event) =>
                  updateAvailability(spirit.id, !event.currentTarget.checked)
                }
              />
            )}
            <div style={{ flex: 1 }}>
              {!isArchive && (
                <Tooltip label={t("delBeer")}>
                  <ActionIcon variant="subtile" onClick={() => unsetBeer(beer)}>
                    <IconTrash />
                  </ActionIcon>
                </Tooltip>
              )}
            </div>
            <Tooltip
              label={isArchive ? t("moveOutOfGraveyard") : t("moveToGraveyard")}
              withArrow
            >
              <ActionIcon variant="subtle" onClick={toggleArchive}>
                <Icon
                  icon={isArchive ? ICONS.INBOX_UP : ICONS.INBOX_DOWN}
                  color={getIconColor(theme)}
                />
              </ActionIcon>
            </Tooltip>
          </Group>
        )}
      </div>
    </Card>
  );
}
