import { useState } from "react";

import { useSession } from "next-auth/react";

import { useTranslation } from "next-i18next";

import {
  Avatar,
  Button,
  Container,
  Grid,
  Group,
  Modal,
  Paper,
  Space,
} from "@mantine/core";

import { Icon, ICONS } from "vseth-canine-ui";

import dayjs from "dayjs";
var relativeTime = require("dayjs/plugin/relativeTime");

import hasAccess from "../utilities/hasAccess";

import { gql, useMutation } from "@apollo/client";

const deleteCommentMut = gql`
  mutation deleteComment($id: Int!) {
    deleteComment(id: $id)
  }
`;

export default function BeerComments({ comments, refetch }) {
  dayjs.extend(relativeTime);
  const { t } = useTranslation("common");

  const [opened, setOpened] = useState(false);
  const [commentToDelete, setCommentToDelete] = useState(null);

  const { data: session } = useSession();

  const [deleteComment, { loading, error }] = useMutation(deleteCommentMut);

  const getInitials = (name) => {
    return name
      .split(" ")
      .map((n) => n[0])
      .join("");
  };

  const openModal = (comment) => {
    setOpened(true);
    setCommentToDelete(comment);
  };

  const removeComment = async () => {
    if (commentToDelete) {
      await deleteComment({
        variables: {
          id: commentToDelete.id,
        },
      });
      refetch();
      setOpened(false);
    }
  };

  return (
    <Container size="xl">
      {comments && (
        <Grid>
          {comments.map((comment) => (
            <Grid.Col
              md={4}
              sm={6}
              xs={12}
              style={{ marginTop: "20px" }}
              key={comment.id}
            >
              <div>
                <Group>
                  {hasAccess(session, true) ? (
                    <Avatar
                      radius="sm"
                      color="red"
                      size="md"
                      onClick={() => openModal(comment)}
                      style={{ cursor: "pointer" }}
                    >
                      <Icon icon={ICONS.DELETE} color="red" />
                    </Avatar>
                  ) : (
                    <Avatar radius="sm" color="vsethMain" size="md">
                      {getInitials(comment.submitter.name)}
                    </Avatar>
                  )}
                  <div>
                    <p style={{ marginBottom: "-10px" }}>
                      {comment.submitter.name}
                    </p>
                    <span style={{ color: "gray", fontSize: "10pt" }}>
                      {dayjs.unix(Number(comment.date) / 1000).fromNow()}
                    </span>
                  </div>
                </Group>
                <p style={{ marginTop: "5px" }}>{comment.comment}</p>
              </div>
            </Grid.Col>
          ))}
        </Grid>
      )}
      <Modal
        opened={opened}
        onClose={() => setOpened(false)}
        title={<b>{t("removeCommentTitle")}</b>}
        centered
      >
        {t("removeCommentQuestion")}
        {commentToDelete && (
          <Paper shadow="xl" p="md" mt="sm" mb="sm">
            <Group>
              <Avatar radius="sm" color="vsethMain" size="md">
                {getInitials(commentToDelete.submitter.name)}
              </Avatar>
              <div>
                <p style={{ marginBottom: "-10px" }}>
                  {commentToDelete.submitter.name}
                </p>
                <span style={{ color: "gray", fontSize: "10pt" }}>
                  {dayjs.unix(Number(commentToDelete.date) / 1000).fromNow()}
                </span>
              </div>
            </Group>
            <p style={{ marginTop: "5px" }}>{commentToDelete.comment}</p>
          </Paper>
        )}
        <Button
          color="red"
          onClick={removeComment}
          leftIcon={<Icon icon={ICONS.DELETE} color="white" />}
        >
          {t("removeCommentButton")}
        </Button>
      </Modal>
    </Container>
  );
}
