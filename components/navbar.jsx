import React, { ReactNode, useState, useEffect } from "react";
import Link from "next/link";
import { useRouter } from "next/router";

import { useTranslation } from "next-i18next";
import { useSession } from "next-auth/react";

import { Notifications } from "@mantine/notifications";

import { ApolloProvider } from "@apollo/client";
import apolloClient from "../lib/apollo";

import { ColorSchemeProvider, Group, MediaQuery } from "@mantine/core";

import {
  makeVsethTheme,
  useConfig,
  VSETHExternalApp,
  VSETHThemeProvider,
} from "vseth-canine-ui";

import hasAccess from "../utilities/hasAccess";

import LoginButton from "../components/loginButton";
import ThemeSwitcher from "../components/themeSwitcher";

export default function Navbar({ children }) {
  const router = useRouter();
  const { locale, pathname, asPath, query } = router;
  const { t } = useTranslation("common");
  const { data: session } = useSession();

  const theme = makeVsethTheme();
  const [selectedLanguage, setSelectedLanguage] = useState(locale);

  const [colorScheme, setColorScheme] = useState("dark");
  const toggleColorScheme = (value) =>
    setColorScheme(value || (colorScheme === "dark" ? "light" : "dark"));
  theme.colorScheme = colorScheme;

  /*
   * VSETH Colors: See https://s.aschoch.ch/canine-colors
   * theme.colors.vsethMain[7] is #009fe3 (cyan)
   */
  theme.colors.vsethMain[7] = "#4f2a17";
  theme.colors.vsethMain[0] = "#f2d6c9"; // light color
  theme.colors.vsethMain[1] = "#f2d6c9"; // light color
  theme.colors.vsethMain[8] = "#824e34"; // light color

  theme.colors.vsethMain[2] = "#ffffff"; // light color

  // NOTE: This is a hack, as colors are hardcoded in vseth-canine
  useEffect(() => {
    const footerDiv = document.querySelector("footer>div");
    const footerLogo = document.querySelector("footer img");
    const footerVSUZH = document.querySelector(".mantine-92obeb");
    const selectItems = document.querySelector("header a");

    if (
      pathname !== "/beerMenu" &&
      pathname !== "/drinksMenu" &&
      pathname !== "/beerMenuSpider"
    ) {
      if (colorScheme == "dark") {
        footerDiv.classList.add("vseth-footer-dark");
        footerLogo.classList.add("vseth-logo-dark");
        footerVSUZH.classList.add("vsuzh-logo-dark");
        if (selectItems) selectItems.classList.add("vseth-text-dark");
      } else {
        footerDiv.classList.remove("vseth-footer-dark");
        footerLogo.classList.remove("vseth-logo-dark");
        footerVSUZH.classList.remove("vsuzh-logo-dark");
        if (selectItems) selectItems.classList.remove("vseth-text-dark");
      }
    }
  }, [colorScheme]);

  const { data } = useConfig(
    "https://static.vseth.ethz.ch/assets/vseth-0516-pub/config.json"
  );

  const customDemoNav = [
    { title: t("main"), href: "/" },
    { title: t("blog"), href: "/blog" },
    { title: t("about"), href: "/about" },
  ];

  if (hasAccess(session, true)) {
    customDemoNav.push({ title: t("menu"), href: "/beerMenu" });
    customDemoNav.push({ title: "Spider", href: "/beerMenuSpider" });
    customDemoNav.push({ title: t("drinksMenu"), href: "/drinksMenu" });
    customDemoNav.push({ title: t("graveyard"), href: "/graveyard" });
  }

  return (
    <React.StrictMode>
      {pathname !== "/beerMenu" &&
      pathname !== "/drinksMenu" &&
      pathname !== "/beerMenuSpider" ? (
        <ColorSchemeProvider
          colorScheme={colorScheme}
          toggleColorScheme={toggleColorScheme}
        >
          <VSETHThemeProvider theme={theme} withGlobalStyles withNormalizeCSS>
            <VSETHExternalApp
              selectedLanguage={selectedLanguage}
              onLanguageSelect={(lang) => {
                setSelectedLanguage(lang);
                router.push({ pathname, query }, asPath, { locale: lang });
              }}
              languages={data?.languages}
              title={"PapperlaWeb"}
              appNav={customDemoNav}
              organizationNav={data.externalNav}
              makeWrapper={(url, child) => (
                <>
                  <MediaQuery smallerThan="md" styles={{ display: "none" }}>
                    <Link
                      href={url}
                      style={{
                        textDecoration: "none",
                        color:
                          theme.colorScheme == "light"
                            ? "#333333b3"
                            : "#C1C2C5",
                      }}
                    >
                      {child}
                    </Link>
                  </MediaQuery>
                  <MediaQuery largerThan="md" styles={{ display: "none" }}>
                    <Link
                      href={url}
                      style={{
                        textDecoration: "none",
                        color: "#C1C2C5",
                      }}
                    >
                      {child}
                    </Link>
                  </MediaQuery>
                </>
              )}
              privacyPolicy={data?.privacy}
              disclaimer={data?.copyright}
              //activeHref is the current active url path. Required to style the active page in the Nav
              activeHref={"/"}
              socialMedia={data?.socialMedia}
              logo={"/pub-logo.png"}
              loginButton={
                <Group>
                  <ThemeSwitcher />
                  <LoginButton />
                </Group>
              }
              signet={"/pub-signet.png"}
              size="xl"
              showVSUZHLogo={true}
              styles={(theme) => ({
                root: {
                  color: "red",
                },
              })}
            >
              <ApolloProvider client={apolloClient}>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    height: "100%",
                  }}
                >
                  {children}
                </div>
              </ApolloProvider>
            </VSETHExternalApp>
          </VSETHThemeProvider>
          <Notifications />
        </ColorSchemeProvider>
      ) : (
        <ApolloProvider client={apolloClient}>{children}</ApolloProvider>
      )}
    </React.StrictMode>
  );
}
