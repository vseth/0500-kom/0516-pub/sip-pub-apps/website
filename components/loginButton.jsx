import { useSession, signIn, signOut } from "next-auth/react";

import { Button, useMantineTheme } from "@mantine/core";

import { Icon, ICONS } from "vseth-canine-ui";

import { getIconColor } from "../utilities/colors";

const LoginButton = () => {
  const { data: session } = useSession();
  const theme = useMantineTheme();

  if (session) {
    return (
      <Button
        leftIcon={<Icon icon={ICONS.LEAVE} color={getIconColor(theme)} />}
        variant="light"
        onClick={() => signOut()}
      >
        Logout
      </Button>
    );
  }
  return (
    <Button
      leftIcon={<Icon icon={ICONS.ENTER} color={getIconColor(theme)} />}
      variant="light"
      onClick={() => signIn("keycloak")}
    >
      Login
    </Button>
  );
};

export default LoginButton;
