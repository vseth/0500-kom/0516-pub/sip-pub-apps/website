import { useState, useEffect } from "react";
import { useSession, signIn, signOut } from "next-auth/react";

import { useTranslation } from "next-i18next";

import { Accordion, Box, Button, Container, Space } from "@mantine/core";

/*
 * @mantine/core's Rating component has a bug on mobile that it is not
 * displaying its value correctly. For example, if the value is 4, the
 * component might display 3.5 stars.
 */
import CircularSlider from "@fseehawer/react-circular-slider";

import { Icon, ICONS } from "vseth-canine-ui";

import PleaseLogIn from "../components/pleaseLogIn";

import { gql, useMutation } from "@apollo/client";

const addOrUpdateRatingMutation = gql`
  mutation addOrUpdateRating($beerId: Int, $rating: Float) {
    addOrUpdateRating(beerId: $beerId, rating: $rating)
  }
`;

export default function BeerRating({
  beerId,
  refetch,
  ratings,
  notify,
  update,
}) {
  const { t } = useTranslation("common");

  const { data: session } = useSession();
  const [addOrUpdateRating, { loading, error }] = useMutation(
    addOrUpdateRatingMutation
  );

  const [rating, setRating] = useState(0);
  const [found, setFound] = useState([]);
  const [foundOne, setFoundOne] = useState(false);

  const reloadRating = () => {
    if (session && ratings) {
      const foundLoc = ratings.filter(
        (rating) => rating.user.sub == session.info.payload.sub
      );
      setFound(foundLoc);
      if (foundLoc.length > 0) {
        setRating(foundLoc[0].rating);
        setFoundOne(true);
      }
    }
  };

  useEffect(() => {
    reloadRating();
  }, [ratings]);

  const submit = async () => {
    await addOrUpdateRating({
      variables: {
        beerId,
        rating,
      },
    });
    notify(t("ratingUpdated"), "", "green");

    await refetch();
  };

  return (
    <Accordion.Item value="rating">
      <Accordion.Control>
        <h2 style={{ margin: 0 }}>{t("rateThisBeer")}</h2>
      </Accordion.Control>
      <Accordion.Panel>
        <Space h="md" />
        {session ? (
          <div style={{ textAlign: "center" }}>
            <Box>
              <CircularSlider
                onChange={(val) => setRating(val)}
                label={t("rating")}
                min={0}
                max={5}
                data={Array(51)
                  .fill(1)
                  .map((element, index) => index / 10)}
                dataIndex={Math.round(rating * 10)}
                knobColor="#ad6300"
                progressColorFrom="#ff8500"
                progressColorTo="#ad6300"
                progressSize={8}
                trackColor="#fcefde"
                labelColor="#ad6300"
              />
            </Box>
            <Button style={{ marginTop: "10px" }} onClick={submit}>
              {foundOne ? t("update") : t("submit")}
            </Button>
          </div>
        ) : (
          <>
            <PleaseLogIn />
          </>
        )}
        <Space h="md" />
      </Accordion.Panel>
    </Accordion.Item>
  );
}
