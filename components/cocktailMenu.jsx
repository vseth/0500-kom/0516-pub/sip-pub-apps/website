import { useState } from "react";

import {
  Button,
  Grid,
  SegmentedControl,
  Space,
  TextInput,
  useMantineTheme,
} from "@mantine/core";

import { useDebouncedState } from "@mantine/hooks";

import CocktailCard from "../components/cocktailCard";
import AddCocktailModal from "../components/addCocktailModal";
import IngredientsList from "../components/ingredientsList";

import hasAccess from "../utilities/hasAccess";
import isAvailable from "../utilities/isAvailable";
import { getIconColor } from "../utilities/colors";

import { useTranslation } from "next-i18next";
import { useSession } from "next-auth/react";

import { Icon, ICONS } from "vseth-canine-ui";

import { gql, useQuery, useMutation } from "@apollo/client";

export default function CocktailMenu({
  cocktails,
  refetch,
  isArchive = false,
}) {
  const [selection, setSelection] = useState(null);
  const [modalOpen, setModalOpen] = useState(false);
  const [filter, setFilter] = useDebouncedState("", 200);
  const [segCon, setSegCon] = useState("ingredientsListView");

  const { data: session } = useSession();
  const { t } = useTranslation("common");

  const theme = useMantineTheme();

  const edit = (cocktail) => {
    setSelection(cocktail);
    setModalOpen(true);
  };

  const add = () => {
    setSelection(null);
    setModalOpen(true);
  };

  const matchesCocktailsFilter = (cocktail) => {
    const combinedString =
      cocktail.name.toLowerCase() +
      " " +
      cocktail.tags.map((tag) => tag.tag.name.toLowerCase()).join(" ") +
      " " +
      cocktail.ingredients
        .map((ingredient) => ingredient.ingredient.name.toLowerCase())
        .join(" ");
    return combinedString.includes(filter.toLowerCase());
  };

  return (
    <>
      <div style={{ textAlign: "center" }}>
        <h1>{t("drinks")}</h1>
        {hasAccess(session, true) && !isArchive && (
          <>
            <Button
              leftIcon={<Icon icon={ICONS.PLUS} color="white" />}
              onClick={add}
            >
              {t("addCocktail")}
            </Button>

            <Space h="md" />
            <SegmentedControl
              value={segCon}
              onChange={setSegCon}
              data={[
                {
                  label: t("ingredientsListView"),
                  value: "ingredientsListView",
                },
                {
                  label: t("cocktailIngredientsView"),
                  value: "cocktailIngredientsView",
                },
              ]}
            />
            <Space h="md" />
          </>
        )}
      </div>

      <TextInput
        defaultValue={filter}
        onChange={(e) => setFilter(e.target.value)}
        icon={<Icon icon={ICONS.SEARCH} color={getIconColor(theme)} />}
        placeholder={t("search")}
        size="md"
      />

      <Space h="md" />
      <Grid>
        {cocktails &&
          cocktails
            .filter(
              (cocktail) =>
                (isAvailable(cocktail) || hasAccess(session, true)) &&
                matchesCocktailsFilter(cocktail)
            )
            .map((cocktail) => (
              <Grid.Col
                xs={12}
                sm={6}
                md={4}
                key={cocktail.id}
                style={{ display: "flex" }}
              >
                <CocktailCard
                  cocktail={cocktail}
                  editCocktail={edit}
                  refetch={refetch}
                  isArchive={isArchive}
                  showIngredientsList={segCon == "cocktailIngredientsView"}
                />
              </Grid.Col>
            ))}
      </Grid>

      {hasAccess(session, true) &&
        !isArchive &&
        segCon == "ingredientsListView" && (
          <IngredientsList refetch={refetch} />
        )}

      <AddCocktailModal
        open={modalOpen}
        close={() => setModalOpen(false)}
        cocktail={selection}
        refetch={refetch}
      />
    </>
  );
}
