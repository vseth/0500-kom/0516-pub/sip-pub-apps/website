import { useEffect } from "react";

import { Box, Divider, Grid, Group } from "@mantine/core";

import { Icon, ICONS } from "vseth-canine-ui";

import { IconBottleOff, IconBarrel } from "@tabler/icons-react";

import config from "../papperlaweb.config";

import { formatPrice, formatIngredients } from "../utilities/dataFormatter";
import { getColor } from "../utilities/dataFormatter";

import { gql, useQuery } from "@apollo/client";

const getAvailableBeersQuery = gql`
  {
    getAvailableBeers {
      id
      name
      type
      brewery
      price
      volume
      category
      alcohol
      onTap
      menuOverride
    }
  }
`;

const getAvailableCocktailsQuery = gql`
  {
    getAvailableCocktails {
      name
      price
      containsAlcohol
      ingredients {
        ingredient {
          name
        }
      }
    }
  }
`;

const getAvailableSpiritsQuery = gql`
  {
    getAvailableSpirits {
      name
      type
      price
      alcohol
    }
  }
`;

function BeerMenu() {
  const { data: beers, refetch } = useQuery(getAvailableBeersQuery);
  const { data: cocktails, refetch: refetchC } = useQuery(
    getAvailableCocktailsQuery
  );
  const { data: spirits, refetch: refetchS } = useQuery(
    getAvailableSpiritsQuery
  );

  useEffect(() => {
    const interval = setInterval(() => {
      refetch();
    }, config.refetchFrequency * 1000);

    return () => clearInterval(interval);
  }, []);

  return (
    <Box
      style={{
        width: "100%",
        height: "100%",
        position: "absolute",
        margin: 0,
        backgroundColor: "black",
        zIndex: "-20",
        overflowY: "hidden",
      }}
    >
      <img
        style={{
          zIndex: "-10",
          position: "absolute",
          width: "50%",
          left: "25%",
        }}
        src="/background.png"
      />
      <Grid
        style={{
          width: "100%",
          margin: "auto",
          height: "100%",
          justifyContent: "space-evenly",
        }}
      >
        <Grid.Col md={5} style={{ textAlign: "center" }}>
          <h1
            style={{
              color: "white",
              marginBottom: -36,
              marginTop: "-1rem",
            }}
          >
            Beers
          </h1>

          <table style={{ color: "white", textAlign: "left", width: "100%" }}>
            <thead style={{ color: "yellow" }}>
              <tr>
                <td></td>
                <td></td>
                <td>
                  <h2>Name</h2>
                </td>
                <td>
                  <h2>Style</h2>
                </td>
                <td>
                  <h2>cL</h2>
                </td>
                <td>
                  <h2>CHF</h2>
                </td>
              </tr>
            </thead>
            {beers && (
              <tbody style={{ fontFamily: "Futura", fontSize: "2rem" }}>
                {beers.getAvailableBeers.map((beer, i) => (
                  <tr style={{ color: getColor(beer.category, true) }} key={i}>
                    <td>
                      <Group
                        style={{
                          width: "1.6rem",
                          height: "1.6rem",
                          marginRight: "1rem",
                          marginLeft: "auto",
                        }}
                      >
                        {beer.category == "alcohol-free" && (
                          <svg viewBox="0 0 135.62428 135.80687">
                            <g transform="translate(-37.306249,-80.697919)">
                              <path
                                style={{
                                  fill: getColor(beer.category, true),
                                  height: 12,
                                }}
                                d="M 99.868085,216.13915 C 36.521223,208.81006 14.711294,131.63672 65.332492,93.93668 115.58119,56.51406 186.19846,104.18541 170.77251,165.11569 c -7.98857,31.55372 -40.01149,54.5977 -70.904425,51.02346 z m 13.890615,-8.438 c 39.62133,-6.10879 62.50506,-48.46911 45.42773,-84.09176 -2.26308,-4.72069 -7.141,-12.20408 -8.91438,-13.67585 -0.40138,-0.33312 -84.006047,82.67405 -84.006047,83.40568 0,0.10757 0.922734,0.93219 2.05052,1.83251 12.829702,10.242 29.846602,14.93394 45.442177,12.52942 z M 89.284755,188.71169 c 0,-7.47531 0,-7.47531 15.874995,-23.34865 15.87499,-15.87335 15.87499,-15.87335 15.87499,7.4753 0,23.34865 0,23.34865 -15.87499,23.34865 -15.874995,0 -15.874995,0 -15.874995,-7.4753 z M 74.930204,173.10301 c 14.354551,-14.35274 14.354551,-14.35274 14.359141,-20.43906 0.007,-9.67158 1.15054,-13.17772 5.86449,-17.98551 2.06842,-2.1096 2.06842,-2.1096 2.06842,-8.00364 0,-5.89405 0,-5.89405 7.937495,-5.89405 7.9375,0 7.9375,0 7.94581,6.01927 0.0125,9.05656 -1.85219,9.98193 16.13433,-8.00671 13.23835,-13.2399 14.86787,-14.95857 14.54806,-15.34393 -3.85391,-4.64368 -17.96247,-11.61549 -27.25112,-13.46627 -47.763855,-9.51705 -86.510606,40.54464 -65.100373,84.11118 2.392355,4.86807 8.078263,13.36145 8.944849,13.36145 0.106893,0 6.653897,-6.45873 14.548898,-14.35273 z m 22.292051,-68.19726 c 0,-7.9375 0,-7.9375 7.937495,-7.9375 7.9375,0 7.9375,0 7.9375,7.9375 0,7.9375 0,7.9375 -7.9375,7.9375 -7.937495,0 -7.937495,0 -7.937495,-7.9375 z"
                              />
                            </g>
                          </svg>
                        )}
                        {beer.onTap && (
                          <svg viewBox="0 0 119.54623 135.37498">
                            <g transform="translate(-45.508333,-80.697917)">
                              <path
                                style={{
                                  fill: getColor(beer.category, true),
                                  height: 12,
                                }}
                                d="m 45.525548,205.55571 c 0.133941,-13.43012 1.06925,-17.22912 5.781186,-23.48177 6.551212,-8.69333 13.786353,-11.77264 27.667533,-11.77544 7.366772,-10e-4 7.366772,-10e-4 8.851337,-1.9245 0.816511,-1.05766 2.150331,-2.45598 2.964044,-3.10737 1.846964,-1.47853 2.095973,2.79062 -1.959554,-33.59581 -3.818658,-34.261231 -3.761169,-33.356564 -2.375644,-37.383663 5.674937,-16.494491 28.2502,-18.475557 36.20913,-3.17749 3.04183,5.846776 3.07759,4.465093 -1.07123,41.388763 -3.52671,31.38699 -3.52671,31.38699 -1.89777,32.72739 0.89592,0.73722 2.26963,2.1805 3.05268,3.20728 3.32037,4.35383 18.49482,2.62932 18.49482,-2.10186 0,-3.96875 0,-3.96875 7.93749,-3.96875 7.9375,0 7.9375,0 7.9375,-5.15938 0,-5.15937 0,-5.15937 3.96875,-5.15937 3.96875,0 3.96875,0 3.96875,30.0302 0,30.03021 0,30.03021 -3.96875,30.03021 -3.96875,0 -3.96875,0 -3.96875,-5.02708 0,-5.02709 0,-5.02709 -7.9375,-5.02709 -7.93749,0 -7.93749,0 -7.93749,-3.96875 0,-3.96875 0,-3.96875 -8.70295,-3.96875 -6.73251,0 -8.73176,0.0751 -8.83024,0.33175 -1.19047,3.1023 -7.93782,7.88299 -13.02619,9.22941 -8.4601,2.23862 -19.519666,-1.87688 -23.598275,-8.78142 -0.460604,-0.77974 -0.460604,-0.77974 -6.951995,-0.77392 -10.822731,0.01 -10.853227,0.0474 -10.85554,13.42179 -0.0015,8.53281 -0.0015,8.53281 -11.928854,8.53281 -11.927378,0 -11.927378,0 -11.822488,-10.51719 z m 15.816278,-2.38125 c 0.0099,-12.52759 4.38422,-16.98894 16.664838,-16.99628 5.159375,-0.003 5.159375,-0.003 5.159375,-3.99534 0,-7.03572 -16.423576,-5.17215 -23.82508,2.7034 -7.354234,7.82526 -8.679028,23.24916 -1.996924,23.24916 3.99388,0 3.99388,0 3.997791,-4.96094 z m 48.301624,-7.50422 c 6.3986,-1.95796 10.68139,-8.86127 9.69521,-15.62743 -1.6837,-11.55189 -15.87952,-16.20694 -24.156422,-7.92129 -10.479909,10.49097 0.215737,27.90782 14.461212,23.54872 z m 47.47362,-13.46401 c 0,-11.90624 0,-11.90624 -3.96875,-11.90624 -3.96875,0 -3.96875,0 -3.96875,11.90624 0,11.90625 0,11.90625 3.96875,11.90625 3.96875,0 3.96875,0 3.96875,-11.90625 z m -15.87499,0 c 0,-3.96874 0,-3.96874 -6.94532,-3.96867 -6.94531,7e-5 -6.94531,7e-5 -6.94531,3.96877 0,3.9687 0,3.9687 6.94531,3.96868 6.94532,-3e-5 6.94532,-3e-5 6.94532,-3.96878 z m -35.98334,-22.0927 c 1.96454,0 3.93692,0.0819 4.38307,0.18208 1.5217,0.34156 7.94631,-60.28343 6.74932,-63.689078 -4.03448,-11.478824 -20.831868,-10.106313 -22.752258,1.859082 -0.573197,3.571426 6.270163,62.046926 7.235948,61.830166 0.44663,-0.10024 2.41939,-0.18225 4.38392,-0.18225 z"
                              />
                            </g>
                          </svg>
                        )}
                      </Group>
                    </td>
                    <td>{i}</td>
                    <td>
                      {beer.menuOverride
                        ? beer.menuOverride
                        : beer.brewery + ": " + beer.name}
                    </td>
                    <td valign="top">{beer.type}</td>
                    <td valign="top">{beer.volume}&nbsp;&nbsp;</td>
                    <td valign="top">{formatPrice(beer.price)}</td>
                  </tr>
                ))}
              </tbody>
            )}
          </table>
          <>
            <Divider
              my="xs"
              label={<Icon icon={ICONS.BEER} color="white" size={20} />}
              labelPosition="center"
            />
            <p
              style={{
                fontFamily: "futura",
                fontSize: "2rem",
                color: "#ec008c",
              }}
            >
              15th Anniversary on 10.- 14. & 17.-21. March!
            </p>
          </>
          {config.showDepotText && (
            <>
              <Divider
                my="xs"
                label={<Icon icon={ICONS.BEER} color="white" size={20} />}
                labelPosition="center"
              />
              <p
                style={{
                  fontFamily: "futura",
                  fontSize: "2rem",
                  color: "#ec008c",
                }}
              >
                2CHF Depot für Gläser und Flaschen
              </p>
            </>
          )}
        </Grid.Col>
      </Grid>
    </Box>
  );
}

export default BeerMenu;
