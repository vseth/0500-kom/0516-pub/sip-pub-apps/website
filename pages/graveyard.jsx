import { useRouter } from "next/router";

import BeerMenu from "../components/beerMenu";
import CocktailMenu from "../components/cocktailMenu";
import SpiritMenu from "../components/spiritMenu";

import Unauthorized from "../components/unauthorized";

import { Container } from "@mantine/core";

import hasAccess from "../utilities/hasAccess";
import { useSession } from "next-auth/react";

import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import { gql, useQuery } from "@apollo/client";

const getBeersQuery = gql`
  {
    getArchivedBeers {
      id
      name
      type
      brewery
      price
      volume
      category
      isAvailable
      isArchived
      alcohol
      onTap
      menuOverride
      ratings {
        rating
        user {
          sub
        }
      }
    }
  }
`;

const getCocktailsQuery = gql`
  {
    getArchivedCocktails {
      id
      name
      price
      containsAlcohol
      ingredients {
        ingredient {
          name
          isAvailable
        }
      }
      tags {
        tag {
          name
        }
      }
      isArchived
    }
  }
`;

const getSpiritsQuery = gql`
  {
    getArchivedSpirits {
      id
      name
      type
      price
      alcohol
      tags {
        tag {
          name
        }
      }
      isAvailable
      isArchived
    }
  }
`;

function Graveyard() {
  const { data: beers, refetch: refetchB } = useQuery(getBeersQuery);
  const { data: cocktails, refetch: refetchC } = useQuery(getCocktailsQuery);
  const { data: spirits, refetch: refetchS } = useQuery(getSpiritsQuery);

  const router = useRouter();
  const { data: session } = useSession();

  if (!hasAccess(session, true)) return <Unauthorized />;

  return (
    <Container size="xl" style={{ width: "100%" }}>
      <BeerMenu
        beers={beers?.getArchivedBeers}
        refetch={refetchB}
        isArchive={true}
      />
      <CocktailMenu
        cocktails={cocktails?.getArchivedCocktails}
        refetch={refetchC}
        isArchive={true}
      />
      <SpiritMenu
        spirits={spirits?.getArchivedSpirits}
        refetch={refetchS}
        isArchive={true}
      />
    </Container>
  );
}

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      protected: false,
    },
  };
}

export default Graveyard;
