import aws from "aws-sdk";

export const config = {
  api: {
    bodyParser: false,
  },
};

const s3 = new aws.S3({
  // Set your S3 configuration here
  accessKeyId: process.env.SIP_S3_PAPPERLAWEB_ACCESS_KEY,
  secretAccessKey: process.env.SIP_S3_PAPPERLAWEB_SECRET_KEY,
  endpoint: process.env.SIP_S3_PAPPERLAWEB_HOST,
  region: "eu-central-2", // Change to your desired region
});

export default async function handler(req, res) {
  if (req.method === "POST") {
    const resource =
      "arn:aws:s3:::" + process.env.SIP_S3_PAPPERLAWEB_BUCKET + "/*";
    const bucketPolicy = {
      Bucket: process.env.SIP_S3_PAPPERLAWEB_BUCKET,
      Policy: JSON.stringify({
        Version: "2012-10-17",
        Statement: [
          {
            Sid: "PublicReadGetObject",
            Effect: "Allow",
            Principal: "*",
            Action: ["s3:GetObject"],
            Resource: [resource],
          },
        ],
      }),
    };
    s3.putBucketPolicy(bucketPolicy, function (err, data) {
      if (err) {
        console.log("Error", err);
        res.status(500).json({
          status: "error",
          error: err,
        });
      }
    });
    res.status(200).json({
      status: "ok",
    });
  } else {
    res.status(405).json({ error: "Method not allowed" });
  }
}
