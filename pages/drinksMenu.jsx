import { useEffect } from "react";

import { Box, Divider, Grid, Group } from "@mantine/core";

import { Icon, ICONS } from "vseth-canine-ui";

import {
  IconBottleOff,
  IconBarrel,
  IconGlassCocktail,
} from "@tabler/icons-react";

import config from "../papperlaweb.config";

import { formatPrice, formatIngredients } from "../utilities/dataFormatter";
import { getColor } from "../utilities/dataFormatter";

import { gql, useQuery } from "@apollo/client";

const getAvailableBeersQuery = gql`
  {
    getAvailableBeers {
      id
      name
      type
      brewery
      price
      volume
      category
      alcohol
      onTap
      menuOverride
    }
  }
`;

const getAvailableCocktailsQuery = gql`
  {
    getAvailableCocktails {
      name
      price
      containsAlcohol
      ingredients {
        ingredient {
          name
        }
      }
    }
  }
`;

const getAvailableSpiritsQuery = gql`
  {
    getAvailableSpirits {
      name
      type
      price
      alcohol
    }
  }
`;

function BeerMenu() {
  const { data: beers, refetch } = useQuery(getAvailableBeersQuery);
  const { data: cocktails, refetch: refetchC } = useQuery(
    getAvailableCocktailsQuery
  );
  const { data: spirits, refetch: refetchS } = useQuery(
    getAvailableSpiritsQuery
  );

  useEffect(() => {
    const interval = setInterval(() => {
      refetchC();
      refetchS();
    }, config.refetchFrequency * 1000);

    return () => clearInterval(interval);
  }, []);

  return (
    <Box
      style={{
        width: "100%",
        height: "100%",
        position: "absolute",
        margin: 0,
        backgroundColor: "black",
        zIndex: "-20",
        overflowY: "hidden",
      }}
    >
      <img
        style={{
          zIndex: "-10",
          position: "absolute",
          width: "50%",
          left: "25%",
        }}
        src="/background.png"
      />
      <Grid
        style={{
          width: "100%",
          margin: "auto",
          height: "100%",
          justifyContent: "space-evenly",
        }}
      >
        <Grid.Col md={5} style={{ textAlign: "center", color: "white" }}>
          {config.showCocktailsInMenu && (
            <div>
              <h3
                style={{
                  color: "white",
                  marginBottom: -36,
                  marginTop: 16,
                  fontSize: "3rem",
                  fontFamily: "Aniron",
                }}
              >
                Drinks
              </h3>

              <table
                style={{ color: "white", textAlign: "left", width: "100%" }}
              >
                <thead style={{ color: "yellow" }}>
                  <tr>
                    <td>
                      <h2>Name</h2>
                    </td>
                    <td>
                      <h2>Ingredients</h2>
                    </td>
                    <td>
                      <h2>CHF</h2>
                    </td>
                  </tr>
                </thead>
                {cocktails?.getAvailableCocktails && (
                  <tbody style={{ fontFamily: "Futura", fontSize: "2rem" }}>
                    {cocktails.getAvailableCocktails.map((cocktail, i) => (
                      <tr
                        style={{
                          color: cocktail.containsAlcohol ? "white" : "#00ff00",
                        }}
                        key={i}
                      >
                        <td valign="top">{cocktail.name}</td>
                        <td valign="top">
                          {formatIngredients(cocktail.ingredients)}
                        </td>
                        <td valign="top">{formatPrice(cocktail.price)}</td>
                      </tr>
                    ))}
                  </tbody>
                )}
              </table>
            </div>
          )}

          {config.showSpiritsInMenu && (
            <div>
              <h3
                style={{
                  color: "white",
                  marginBottom: -36,
                  marginTop: "-0.2rem",
                  fontSize: "3rem",
                  fontFamily: "Aniron",
                }}
              >
                Spirits
              </h3>

              <table
                style={{
                  color: "white",
                  textAlign: "left",
                  width: "100%",
                  fontSize: 30,
                }}
              >
                <thead style={{ color: "yellow" }}>
                  <tr>
                    <td>
                      <h2>Name</h2>
                    </td>
                    <td>
                      <h2>Type</h2>
                    </td>
                    <td>
                      <h2>%</h2>
                    </td>
                    <td>
                      <h2>CHF / 4cL</h2>
                    </td>
                  </tr>
                </thead>
                {spirits && (
                  <tbody style={{ fontFamily: "Futura", fontSize: "2rem" }}>
                    {spirits.getAvailableSpirits.map((spirit, i) => (
                      <tr style={{ color: "white" }} key={i}>
                        <td valign="top">{spirit.name}</td>
                        <td valign="top">{spirit.type}</td>
                        <td valign="top">
                          <nobr>{spirit.alcohol} %</nobr>
                        </td>
                        <td valign="top">{formatPrice(spirit.price)}</td>
                      </tr>
                    ))}
                  </tbody>
                )}
              </table>
              <>
                <Divider
                  my="xs"
                  label={<Icon icon={ICONS.BEER} color="white" size={20} />}
                  labelPosition="center"
                />
                <p
                  style={{
                    fontFamily: "futura",
                    fontSize: "2rem",
                    color: "#ec008c",
                  }}
                >
                  15th anniversary on 10.- 14. & 17.-21. March (6-12 PM)!
                </p>
              </>
            </div>
          )}
        </Grid.Col>
      </Grid>
    </Box>
  );
}

export default BeerMenu;
