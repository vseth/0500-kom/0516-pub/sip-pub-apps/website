import { GetStaticPaths } from "next";
import { useRouter } from "next/router";

import { useEffect, useState } from "react";

import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import { Accordion, Container, Grid, Space } from "@mantine/core";

import { notifications } from "@mantine/notifications";

import BeerCard from "../../components/beerCard";
import BeerComment from "../../components/beerComment";
import BeerComments from "../../components/beerComments";
import BeerRating from "../../components/beerRating";
import FlavourForm from "../../components/flavourForm";
import FlavourSpider from "../../components/flavourSpider";

import { gql, useQuery } from "@apollo/client";

const getBeerQuery = gql`
  query getBeerById($id: Int) {
    getBeerById(id: $id) {
      id
      name
      type
      brewery
      price
      volume
      category
      alcohol
      isAvailable
      comments {
        id
        comment
        submitter {
          name
        }
        date
      }
      ratings {
        rating
        user {
          sub
        }
      }
      flavours {
        sweet
        sour
        hoppy
        malty
        bitter
        fruity
        user {
          sub
        }
      }
    }
  }
`;

export default function Beer() {
  const { t } = useTranslation("common");
  const { query } = useRouter();
  const { id } = query;

  const [accValue, setAccValue] = useState(null);

  const { data: beer, refetch } = useQuery(getBeerQuery, {
    variables: {
      id: Number(id),
    },
  });

  useEffect(() => {
    const interval = setInterval(() => {
      refetch();
    }, 3000);

    return () => clearInterval(interval);
  }, []);

  const notify = (title, message, color) => {
    notifications.show({
      title,
      message,
      color,
      autoClose: 5000,
    });
  };

  if (!beer) return <></>;

  return (
    <Container size="xl" style={{ width: "100%" }}>
      <div style={{ textAlign: "center" }}>
        <h1>{t("tasting")}</h1>
      </div>

      <Grid>
        <Grid.Col
          md={6}
          sm={12}
          style={{
            display: "flex",
            justifyContent: "center",
            flexDirection: "column",
          }}
        >
          {beer.getBeerById && (
            <>
              <BeerCard
                editBeer={() => {}}
                beer={beer.getBeerById}
                updateAvailability={() => {}}
                refetch={() => {}}
              />
            </>
          )}
        </Grid.Col>
        <Grid.Col
          md={6}
          sm={12}
          style={{
            display: "flex",
            justifyContent: "center",
            flexDirection: "column",
          }}
        >
          {beer.getBeerById.flavours && (
            <FlavourSpider
              flavours={beer.getBeerById.flavours}
              setAccValue={setAccValue}
            />
          )}
        </Grid.Col>
      </Grid>
      <Space h="xl" />
      <Accordion value={accValue} onChange={setAccValue}>
        <FlavourForm
          beerId={beer.getBeerById.id}
          refetch={refetch}
          flavours={beer.getBeerById.flavours}
          notify={notify}
          update={() => {}}
        />
        <BeerComment
          beerId={beer.getBeerById.id}
          refetch={refetch}
          notify={notify}
        />
        <BeerRating
          beerId={beer.getBeerById.id}
          refetch={refetch}
          ratings={beer.getBeerById.ratings}
          notify={notify}
          update={() => {}}
        />
      </Accordion>
      <Space h="md" />
      <BeerComments comments={beer.getBeerById.comments} refetch={refetch} />
    </Container>
  );
}

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      protected: false,
    },
  };
}

export const getStaticPaths = async () => {
  return {
    paths: [], //indicates that no page needs be created at build time
    fallback: "blocking", //indicates the type of fallback
  };
};
