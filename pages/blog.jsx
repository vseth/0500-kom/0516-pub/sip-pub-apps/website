import { useState, useEffect } from "react";

import { Container, Pagination } from "@mantine/core";

import { useEditor } from "@tiptap/react";
import { Link } from "@mantine/tiptap";
import StarterKit from "@tiptap/starter-kit";
import Underline from "@tiptap/extension-underline";

import { useSession } from "next-auth/react";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import { Button, Checkbox, Space, TextInput } from "@mantine/core";

import BlogPost from "../components/blogPost";
import Editor from "../components/editor";
import hasAccess from "../utilities/hasAccess";

import config from "../papperlaweb.config";

import { gql, useQuery, useMutation } from "@apollo/client";

const addPostMutation = gql`
  mutation addPost($title: String, $content: String) {
    addPost(title: $title, content: $content)
  }
`;

const sendPostMutation = gql`
  mutation sendPost($title: String, $content: String, $receivers: [String]) {
    sendPost(title: $title, content: $content, receivers: $receivers)
  }
`;

const getPostsQuery = gql`
  query getPosts($skip: Int, $take: Int) {
    getPosts(skip: $skip, take: $take) {
      id
      title
      content
      submitter
      date
    }
  }
`;

const getPostCountQuery = gql`
  {
    getPostCount
  }
`;

function Blog() {
  const { t } = useTranslation("common");
  const { data: session } = useSession();

  const [title, setTitle] = useState("");
  const [receivers, setReceivers] = useState("");
  const [sendMail, setSendMail] = useState(true);
  const [savePost, setSavePost] = useState(true);
  const [showError, setShowError] = useState(false);
  const [showErrorRec, setShowErrorRec] = useState(false);
  const [page, setPage] = useState(1);

  const { data: posts, refetch } = useQuery(getPostsQuery, {
    variables: {
      skip: (page - 1) * config.blogPostCountPerPage,
      take: config.blogPostCountPerPage,
    },
  });
  const { data: getPostCount, refetch: refetchCount } =
    useQuery(getPostCountQuery);
  const [addPost] = useMutation(addPostMutation);
  const [sendPost] = useMutation(sendPostMutation);

  const content = "";

  const editor = useEditor({
    extensions: [StarterKit, Underline, Link],
    content,
  });

  const submit = async () => {
    if (title == "") {
      setShowError(true);
      return;
    }
    let status = true;
    if (sendMail) {
      if (receivers == "") {
        setShowErrorRec(true);
        return;
      }

      const resMail = await sendPost({
        variables: {
          title,
          content: editor.getHTML(),
          receivers: receivers.split(),
        },
      });
      if (!resMail) status = false;
    }
    if (savePost) {
      const res = await addPost({
        variables: {
          title: title,
          content: editor.getHTML(),
        },
      });
      if (res) refetch();
      else status = false;
    }
    if (status) {
      setTitle("");
      setReceivers("");
      editor.commands.setContent("");
    }
  };

  return (
    <Container size="xl">
      <div style={{ textAlign: "center" }}>
        <h1>{t("blog")}</h1>
      </div>

      {hasAccess(session, true) && (
        <div>
          <TextInput
            value={title}
            onChange={(e) => {
              setTitle(e.target.value);
              setShowError(false);
            }}
            label={t("title")}
            placeholder="Alkoholische Köstlichkeiten"
            size="md"
            error={showError ? t("ENotEmpty") : ""}
            withAsterisk
          />
          <Space h="md" />
          <Editor editor={editor} />
          {sendMail && (
            <>
              <Space h="md" />
              <TextInput
                value={receivers}
                onChange={(e) => {
                  setReceivers(e.target.value);
                  setShowErrorRec(false);
                }}
                label={t("receivers")}
                placeholder="vorstand@papperlapub, gpk@lists.papperlapub.ethz.ch"
                size="md"
                error={showErrorRec ? t("ENotEmpty") : ""}
                withAsterisk
              />
            </>
          )}
          <Space h="md" />
          <Checkbox
            checked={sendMail}
            onChange={() => setSendMail(!sendMail)}
            label={t("sendMail")}
          />
          <Checkbox
            mt="md"
            checked={savePost}
            onChange={() => setSavePost(!savePost)}
            label={t("addBlogPost")}
          />
          <Space h="md" />
          <Button onClick={submit}>{t("addPostButton")}</Button>
        </div>
      )}

      <Space h="xl" />
      {posts &&
        posts.getPosts.map((post) => <BlogPost post={post} key={post.id} />)}
      {getPostCount && (
        <Pagination
          page={page}
          onChange={setPage}
          total={Math.ceil(
            getPostCount.getPostCount / config.blogPostCountPerPage
          )}
          position="center"
        />
      )}
    </Container>
  );
}

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      protected: false,
    },
  };
}

export default Blog;
