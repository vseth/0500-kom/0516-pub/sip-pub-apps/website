import "../styles/globals.css";
import React from "react";
import { useRouter } from "next/router";
import Head from "next/head";

import { SessionProvider } from "next-auth/react";

import { appWithTranslation } from "next-i18next";

import Navbar from "../components/navbar";

function App({ Component, pageProps: { session, ...pageProps } }) {
  return (
    <>
      <Head>
        <meta name="description" content="PapperlaPub Website" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link
          rel="icon"
          href={"https://static.vseth.ethz.ch/assets/vseth-0516-pub/signet.png"}
        />
        <title>PapperlaWeb</title>
      </Head>
      <SessionProvider session={session}>
        <Navbar>
          <Component {...pageProps} />
        </Navbar>
      </SessionProvider>
    </>
  );
}

export default appWithTranslation(App);
