/*
  Warnings:

  - You are about to drop the column `isAvailable` on the `Cocktail` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE `Cocktail` DROP COLUMN `isAvailable`;
