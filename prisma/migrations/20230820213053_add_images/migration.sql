-- AlterTable
ALTER TABLE `Cocktail` ADD COLUMN `imageLandscape` VARCHAR(191) NULL,
    ADD COLUMN `imagePortrait` VARCHAR(191) NULL;
