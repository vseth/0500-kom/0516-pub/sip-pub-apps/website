-- CreateTable
CREATE TABLE `Pub` (
    `id` INTEGER NOT NULL DEFAULT 1,
    `host` VARCHAR(191) NOT NULL,
    `date` VARCHAR(191) NOT NULL,
    `updatedAt` DATETIME(3) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
