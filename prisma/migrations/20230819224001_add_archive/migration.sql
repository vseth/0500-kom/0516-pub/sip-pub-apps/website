-- AlterTable
ALTER TABLE `Beer` ADD COLUMN `isArchived` BOOLEAN NOT NULL DEFAULT false;

-- AlterTable
ALTER TABLE `Cocktail` ADD COLUMN `isArchived` BOOLEAN NOT NULL DEFAULT false;
