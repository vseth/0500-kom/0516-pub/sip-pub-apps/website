// This is your Prisma schema file,
// learn more about it in the docs: https://pris.ly/d/prisma-schema

generator client {
  provider = "prisma-client-js"
}

datasource db {
  provider = "mysql"
  url      = env("DATABASE_URL")
}

model Pub {
  id        Int      @id @default(1)
  host      String
  date      String
  updatedAt DateTime @updatedAt
}

model Beer {
  id           Int     @id @default(autoincrement())
  name         String
  brewery      String
  menuOverride String @default("")
  type         String
  price        Decimal
  volume       Int
  category     String
  isAvailable  Boolean @default(false)
  alcohol      Decimal
  onTap        Boolean @default(false)
  isArchived   Boolean @default(false)
  comments     Comment[]
  ratings      Rating[]
  flavours     Flavour[]
}

model Cocktail {
  id              Int     @id @default(autoincrement())
  name            String
  ingredients     CocktailsOnIngredients[]
  tags            CocktailsOnTags[]
  price           Decimal
  containsAlcohol Boolean @default(true)
  isArchived      Boolean @default(false)
  imagePortrait   String?
  imageLandscape  String?
}

model Spirit {
  id          Int            @id @default(autoincrement())
  name        String
  type        String
  price       Decimal
  alcohol     Decimal
  tags        SpiritsOnTags[]
  isAvailable Boolean        @default(false)
  isArchived  Boolean        @default(false)
}

model Ingredient {
  name        String  @unique
  isAvailable Boolean @default(false)
  cocktails   CocktailsOnIngredients[]
}

model CocktailsOnIngredients {
  cocktail       Cocktail @relation(fields: [cocktailId], references: [id], onDelete: Cascade)
  ingredient     Ingredient @relation(fields: [ingredientName], references: [name])
  cocktailId     Int
  ingredientName String
  @@id([cocktailId, ingredientName])
}

model Tag {
  name      String @unique
  cocktails CocktailsOnTags[]
}

model SpiritTag {
  name    String @unique
  spirits SpiritsOnTags[]
}

model CocktailsOnTags {
  cocktail   Cocktail @relation(fields: [cocktailId], references: [id], onDelete: Cascade)
  tag        Tag @relation(fields: [tagName], references: [name])
  cocktailId Int
  tagName    String
  @@id([cocktailId, tagName])
}

model SpiritsOnTags {
  spirit   Spirit @relation(fields: [spiritId], references: [id], onDelete: Cascade)
  tag      SpiritTag @relation(fields: [tagName], references: [name])
  spiritId Int
  tagName  String
  @@id([spiritId, tagName])
}

model Post {
  id        Int      @id @default(autoincrement())
  submitter String
  date      DateTime @default(now())
  title     String
  content   String   @db.Text
}

model Comment {
  id           Int      @id @default(autoincrement())
  date         DateTime @default(now())
  submitter    User     @relation(fields: [submitterSub], references: [sub], onDelete: Cascade)
  submitterSub String
  comment      String
  beer         Beer     @relation(fields: [beerId], references: [id], onDelete: Cascade)
  beerId       Int
}

model User {
  sub      String    @id
  name     String
  comments Comment[]
  ratings  Rating[]
  flavours Flavour[]
}

model Rating {
  id      Int     @id @default(autoincrement())
  beer    Beer    @relation(fields: [beerId], references: [id], onDelete: Cascade)
  beerId  Int
  user    User    @relation(fields: [userSub], references: [sub], onDelete: Cascade)
  userSub String
  rating  Decimal
}

model Flavour {
  id      Int     @id @default(autoincrement())
  beer    Beer    @relation(fields: [beerId], references: [id], onDelete: Cascade)
  beerId  Int
  user    User    @relation(fields: [userSub], references: [sub], onDelete: Cascade)
  userSub String
  sweet   Decimal
  sour    Decimal
  hoppy   Decimal
  malty   Decimal
  fruity  Decimal
  bitter  Decimal
}
