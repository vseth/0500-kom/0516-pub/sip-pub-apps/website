FROM eu.gcr.io/vseth-public/base:foxtrott

RUN echo "Package: nodejs" >> /etc/apt/preferences.d/preferences \
    && echo "Pin: origin deb.nodesource.com" >> /etc/apt/preferences.d/preferences \
    && echo "Pin-Priority: 1001" >> /etc/apt/preferences.d/preferences
RUN apt install -y ca-certificates curl gnupg
RUN mkdir -p /etc/apt/keyrings
RUN curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg

RUN echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_16.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list
RUN apt update
RUN apt install -y nodejs

RUN mkdir -p /papperlaweb
WORKDIR /papperlaweb
COPY . /papperlaweb
RUN mkdir -p /papperlaweb/public/images

RUN npm install
RUN npm run build

COPY cinit.yml /etc/cinit.d/papperlaweb.yml

ENV HOSTNAME "0.0.0.0"

EXPOSE 3000
