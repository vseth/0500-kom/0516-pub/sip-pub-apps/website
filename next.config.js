/** @type {import('next').NextConfig} */
const { i18n } = require("./next-i18next.config");

const nextConfig = {
  reactStrictMode: true,
  i18n: i18n,
  webpack: (config) => {
    config.resolve.fallback = { fs: false };

    return config;
  },
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "minio-redzone.vseth.ethz.ch",
        port: "",
        pathname: "**",
      },
    ],
  },
};

module.exports = nextConfig;
