export const getNextPub = () => {
  const fsweeks = Array(15)
    .fill()
    .map((element, index) => index + 8);
  const hsweeks = Array(14)
    .fill()
    .map((element, index) => index + 38);
  const year = new Date().getFullYear();
  const easter = Easter(year);
  const weeks = [...fsweeks, ...hsweeks].filter((i) => i !== easter);
  const mondayOfCurrentWeek = getDateOfWeek(new Date().getWeek(), year);
  const wednesdays = [year, year + 1].flatMap((y) =>
    weeks.map((w) => {
      const monday = getDateOfWeek(w, y);
      let wednesday = new Date(monday);
      wednesday.setDate(wednesday.getDate() + 2);
      wednesday.setHours(wednesday.getHours() + 23);
      wednesday.setMinutes(wednesday.getMinutes() + 59);
      return wednesday;
    })
  );
  const next = wednesdays.filter((w) => new Date() < w)[0];
  return `${next.getDate()}.${next.getMonth() + 1}.${next.getFullYear()}`;
};

function Easter(Y) {
  var C = Math.floor(Y / 100);
  var N = Y - 19 * Math.floor(Y / 19);
  var K = Math.floor((C - 17) / 25);
  var I = C - Math.floor(C / 4) - Math.floor((C - K) / 3) + 19 * N + 15;
  I = I - 30 * Math.floor(I / 30);
  I =
    I -
    Math.floor(I / 28) *
      (1 -
        Math.floor(I / 28) *
          Math.floor(29 / (I + 1)) *
          Math.floor((21 - N) / 11));
  var J = Y + Math.floor(Y / 4) + I + 2 - C + Math.floor(C / 4);
  J = J - 7 * Math.floor(J / 7);
  var L = I - J;
  var M = 3 + Math.floor((L + 40) / 44);
  var D = L + 28 - 31 * Math.floor(M / 4);

  return new Date(Y, M - 1, D).getWeek();
}

Date.prototype.getWeek = function () {
  var onejan = new Date(this.getFullYear(), 0, 1);
  var today = new Date(this.getFullYear(), this.getMonth(), this.getDate());
  var dayOfYear = (today - onejan + 86400000) / 86400000;
  return Math.ceil(dayOfYear / 7);
};

function getDateOfWeek(w, y) {
  var d = 1 + (w - 1) * 7; // 1st of January + 7 days for each week

  return new Date(y, 0, d);
}

export function formatDate(d, locale) {
  const dat = new Date(Number(d));
  return dat.toLocaleString(locale || "de");
}
