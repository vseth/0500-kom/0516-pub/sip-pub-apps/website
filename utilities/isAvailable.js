export default function isAvailable(cocktail) {
  if (!cocktail) return false;
  let avail = true;
  cocktail.ingredients.map((ingredient) => {
    if (!ingredient.ingredient.isAvailable) avail = false;
  });
  return avail;
}
